﻿using UnityEngine;
using System.Collections;

public class FreezePowerup : Powerup {

	public Material frozenMaterial;
	public bool isWaterMaterial = true;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		ship.rigidbody.isKinematic = true;
		Transform display = ship.transform.Find("Display");
		if (isWaterMaterial) {
			display.gameObject.AddComponent("WaterSimple");
		}
		Material old = display.renderer.material;
		display.renderer.material = frozenMaterial;
		yield return new WaitForSeconds(duration);
		ship.rigidbody.isKinematic = false;
		display.renderer.material = old;
		if (isWaterMaterial) {
			Destroy(display.GetComponent<WaterSimple>());
		}
		Destroy(gameObject);
	}
	
	override public string GetName() {
		return "Immovable Object";
	}

	override public Color GetColor() {
		return Color.blue;
	}

}
