﻿using UnityEngine;
using System.Collections;

public class ShipControl : MonoBehaviour {

	public int playerNumber = 1;
	public float sailingSpeed = 1.0f;
	public float turningSpeed = 1.0f;
	public float timeForCapsize = 3.0f;
	public bool dead = false;

	string forwardAxis;
	string sideAxis;
	float lifetime = 0.0f;
	float capsizeTime = 0.0f;


	public int playerIndex { get { return playerNumber-1; } }


	void Start () {
		forwardAxis = "Player"+playerNumber+"Vertical";
		sideAxis = "Player"+playerNumber+"Horizontal";
	}

	void Update() {
		if (dead || !Director.main.running) return;
		lifetime += Time.deltaTime;
	}

	void FixedUpdate () {
		if (dead) return;
		if (Director.main.firstPersonMode) {
			TurningBasedMovement();
		} else {
			DirectionalMovement();
		}
		if (Vector3.Dot(transform.up, Vector3.up) < -0.5f && transform.position.y < 1.0f) {
			capsizeTime += Time.deltaTime;
		} else {
			capsizeTime = 0;
		}
		if (capsizeTime > timeForCapsize) {
			Die();
		}
	}

	void TurningBasedMovement() {
		Vector3 inputVec = InputU.InputVec(sideAxis, forwardAxis);
		if (inputVec.magnitude > 0.1f) {
			rigidbody.AddForce((Mathf.Clamp01(inputVec.z) * sailingSpeed) * transform.forward);
			rigidbody.AddForceAtPosition((inputVec.x * turningSpeed)*transform.right, transform.position+(transform.forward*0.5f));
		}
	}

	void DirectionalMovement() {
		Vector3 inputVec = InputU.ClampedInputVec(sideAxis, forwardAxis);
		if (inputVec.magnitude > 0.1f) {
			Vector3 forwardForce = Vector3.Project(inputVec, transform.forward);
			Vector3 sideForce = Vector3.Project(inputVec, transform.right);
			if (Vector3.Dot(forwardForce, transform.forward) > 0) {
				rigidbody.AddForce(forwardForce*sailingSpeed, ForceMode.Acceleration);
			}
			rigidbody.AddForceAtPosition(turningSpeed*sideForce, transform.position+(transform.forward*0.5f));
		}
	}

	public float GetLifetime() {
		return lifetime;
	}

	public void Die() {
		dead = true;
		Director.main.CheckForVictory();
	}
}
