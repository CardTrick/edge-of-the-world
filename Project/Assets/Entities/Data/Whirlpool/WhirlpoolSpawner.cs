﻿using UnityEngine;
using System.Collections;

public class WhirlpoolSpawner : MonoBehaviour {

	public GameObject[] spawnableObstacles;
	public GameObject[] powerups;

	public float minSpawnChance = 0.25f;
	public float maxSpawnChance = 1.0f;
	public float timeUntilMostDangerous = 60.0f;
	public float chanceOfPowerup = 0.10f;

	float totalTime = 0.0f;

	void Start() {
		StartCoroutine(SpawnForever());
	}

	void Update() {
		totalTime += Time.deltaTime;
	}

	IEnumerator SpawnForever() {
		while (true) {
			yield return new WaitForSeconds(4.0f);
			float t = Mathf.Clamp01(totalTime / timeUntilMostDangerous);
			float chanceToSpawn = minSpawnChance + (maxSpawnChance-minSpawnChance)*t;
			if (Random.value < chanceToSpawn) {
				SpawnObject(PickObject());
			}
		}
	}

	void SpawnObject(GameObject prefab) {
		GameObject obj = Instantiate(prefab, transform.position - Vector3.up*2, Quaternion.identity) as GameObject;
		float angle = Random.value * Mathf.PI * 2;
		float mag = Random.value * 0.1f + 0.2f;
		Vector3 forceVector = new Vector3(Mathf.Cos(angle), 0, Mathf.Sin(angle)) * mag * obj.rigidbody.mass;
		MoveToEdge f = obj.AddComponent("MoveToEdge") as MoveToEdge;
		f.force = forceVector;
	}

	GameObject PickObject() {
		float selector = Random.value;
		if (selector < chanceOfPowerup) {
			return powerups[Random.Range(0, powerups.Length)];
		}
		float p = 0;
		float itemProb = spawnableObstacles.Length;
		float maxP = (itemProb/2)*(itemProb+1);
		foreach (GameObject prefab in spawnableObstacles) {
			p += itemProb;
			if (p >= selector*maxP) {
				return prefab;
			}
			itemProb -= 1;
		}
		return spawnableObstacles[0];
	}
}
