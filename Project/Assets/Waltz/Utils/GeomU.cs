﻿using UnityEngine;
using System.Collections.Generic;


public class GeomU {

	// Slerp X and Z, lerp Y.
	public static Vector3 Splerp(Vector3 from, Vector3 to, float t) {
		return (Vector3.Slerp(new Vector3(from.x, 0, from.z), new Vector3(to.x, 0, to.z), t) + 
		        Vector3.up * Mathf.Lerp(from.y, to.y, t));
	}

	// Rotate X and Z towards target, move Y towards target linearly.
	public static Vector3 SpinTowards(Vector3 from, Vector3 to,
	                                  float maxRadiansDelta, float maxMagnitudeDelta, float maxHeightDelta) {
		return (Vector3.RotateTowards(new Vector3(from.x, 0, from.z), new Vector3(to.x, 0, to.z), maxRadiansDelta, maxMagnitudeDelta) +
		        Vector3.up * Mathf.MoveTowards(from.y, to.y, maxHeightDelta));
	}

	// Get the entire bounding box of a Transform and its children.
	public static Bounds GetBoundingBox(Transform t) {
		Vector3 max = t.position;
		Vector3 min = t.position;
		Stack<Transform> nextNodes = new Stack<Transform>();
		nextNodes.Push(t);
		while (nextNodes.Count > 0) {
			Transform next = nextNodes.Pop();
			Collider col = next.collider;
			if (col != null) {
				max = Vector3.Max(max, col.bounds.max);
				min = Vector3.Min(min, col.bounds.min);
			}
			foreach (Transform child in next) {
				nextNodes.Push(child);
			}
		}
		Vector3 size = max - min;
		Vector3 center = min + (size/2);
		return new Bounds(center, size);
	}

	// Return a copy of this vector with Y=0.
	public static Vector3 FlatVec(Vector3 vec) {
		return new Vector3(vec.x, 0, vec.z);
	}
}
