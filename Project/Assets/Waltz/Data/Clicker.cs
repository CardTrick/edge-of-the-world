﻿using UnityEngine;
using System.Collections;


// A device set to "click" at a fixed period.
public class Clicker {

	public readonly float period;
	float elapsed = 0.0f;
	int totalClicks = 0;
	int lastCheckedClicks = 0;


	public Clicker(float period) {
		this.period = period;
	}

	// Advance the clock. Should be done every frame you want this Clicker to be active.
	public void Tick(float deltaTime) {
		elapsed += deltaTime;
		if (elapsed > period) {
			int nowClicks = (int)(elapsed/period);
			totalClicks += nowClicks;
			elapsed = Mathf.Repeat(elapsed, period);
		}
	}

	// A version of Tick that returns clicks.
	public int GetClicks(float deltaTime) {
		Tick(deltaTime);
		return clicks;
	}

	// Returns the number of clicks since last accessing this property. Be warned: this will be reset by "clicked".
	public int clicks {
		get {
			int result = totalClicks - lastCheckedClicks;
			lastCheckedClicks = totalClicks;
			return result;
		}
	}

	// Returns whether the Clicker has clicked since last checking. Equivalent to "clicks > 0".
	public bool clicked {
		get { return clicks > 0; }
	}

}
