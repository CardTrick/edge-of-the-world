﻿using UnityEngine;
using System.Collections;

public class Menu : MonoBehaviour {

	public static string[] axes = new string[] {"Player1Vertical", "Player2Vertical", "Player3Vertical"};
	public static string[] confirmButtons = new string[] {
		"Start",
		"Player1Start",
		"Player2Start",
		"Player3Start",
		"Player1Cannons",
		"Player2Cannons",
		"Player3Cannons",
	};

	public GameObject explainer;
	public int selected = 0;
	public Vector3 cursorOffset = Vector3.zero;

	Transform cursor;


	void Start() {
		cursor = transform.Find("Cursor");
		ChangeSelection(selected);
	}

	void ChangeSelection(int index) {
		MenuOption old = transform.Find("Option"+selected).GetComponent<MenuOption>();
		MenuOption now = transform.Find("Option"+index).GetComponent<MenuOption>();
		cursor.parent = now.transform;
		cursor.localPosition = cursorOffset;
		old.SetUnselected();
		now.SetSelected();
		selected = index;
	}

	void Update() {
		float dir = 0;
		foreach (string axis in axes) {
			dir += Input.GetAxis(axis);
		}
		int next = -1;
		if (dir <= -0.5f) {
			next = selected + 1;
		} else if (dir >= 0.5f) {
			next = selected - 1;
		}
		if (0 <= next && next < transform.childCount) {
			ChangeSelection(next);
		}

		foreach (string confirmButton in confirmButtons) {
			if (Input.GetButtonDown(confirmButton)) {
				ConfirmOption();
				break;
			}
		}
	}

	void ConfirmOption() {
		int level = 0;
		switch (selected) {
		case 0: level = 1; break;
		case 1: level = 2; break;
		default: level = 0; break;
		}
		DontDestroyOnLoad(Instantiate(explainer, Vector3.zero, Quaternion.identity));
		Application.LoadLevel(level);
	}
}
