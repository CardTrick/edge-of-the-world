﻿Shader "Custom/UnlitTransparentColored" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color (RGB) Alpha (A)", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			SetTexture [_MainTex] {
				constantColor [_Color]
				combine constant * texture
			}
		}
	} 
	FallBack "Diffuse"
}
