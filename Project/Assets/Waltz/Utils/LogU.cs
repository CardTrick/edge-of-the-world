﻿using UnityEngine;
using System.Collections;


public class LogU {

	// Alias for Debug.Log
	public static void print(object message) {
		Debug.Log(message);
	}

	// Same as print but does string formatting with string.Format
	public static void printf(string message, params object[] items) {
		Debug.Log(string.Format(message, items));
	}
}
