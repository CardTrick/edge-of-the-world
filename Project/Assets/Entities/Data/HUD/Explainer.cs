﻿using UnityEngine;
using System.Collections;

public class Explainer : MonoBehaviour {

	public static string[] startButtons = new string[] {
		"Start", "Player1Start", "Player2Start", "Player3Start",
	};

	bool on = false;

	void OnLevelWasLoaded() {
		renderer.enabled = true;
		on = true;
		Destroy(gameObject, 6.0f);
	}

	void Update() {
		if (!on) return;
		foreach (string start in startButtons) {
			if (Input.GetButtonDown(start)) {
				Destroy(gameObject);
			}
		}
	}
}
