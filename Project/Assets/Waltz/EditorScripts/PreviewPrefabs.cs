﻿using UnityEngine;
using UnityEditor;


public class PreviewPrefabs: ScriptableObject {

	[MenuItem("Waltz/Preview Inner Prefabs &P", false, 8)]
	public static void PreviewAbstractInnerPrefabs() {
		AbstractInnerPrefab[] prefabs = Object.FindObjectsOfType<AbstractInnerPrefab>();
		foreach (AbstractInnerPrefab prefab in prefabs) prefab.Spawn();
	}

	[MenuItem("Waltz/Clear Prefab Previews #&P", false, 9)]
	public static void ClearPrefabPreviews() {
		AbstractInnerPrefab[] prefabs = Object.FindObjectsOfType<AbstractInnerPrefab>();
		foreach (AbstractInnerPrefab prefab in prefabs) prefab.Unspawn();
	}
}
