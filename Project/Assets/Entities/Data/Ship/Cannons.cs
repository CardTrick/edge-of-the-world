﻿using UnityEngine;
using System.Collections;

public class Cannons : MonoBehaviour {

	public GameObject cannonballPrefab;

	public float force = 10.0f;
	public float reloadTime = 3.0f;
	public string fireButton = "Player1Cannons";
	public float fireDelay = 0.1f;

	ShipControl player;
	float reloading = 0;


	void Start() {
		player = GetComponent<ShipControl>();
	}

	void Update() {
		if (player.dead) return;
		if (Input.GetButtonDown(fireButton) || Input.GetAxis(fireButton) > 0.5f) {
			if (reloading <= 0) {
				StartCoroutine(Fire(-1));
				StartCoroutine(Fire(1));
				reloading = reloadTime;
			}
		}
		reloading -= Time.deltaTime;
	}

	IEnumerator Fire(int side) {
		audio.Play();
		for (int i=2; i>=0; i--) {
			Vector3 basePos = transform.position + transform.forward*0.3f + transform.right*side*0.3f + transform.up*0.15f;
			Vector3 vel = (transform.right*side + transform.up*0.2f)*force;
			Vector3 pos = basePos - transform.forward*i*0.25f;
			GameObject ball = Instantiate(cannonballPrefab, pos, transform.rotation) as GameObject;
			ball.rigidbody.velocity = vel;
			yield return new WaitForSeconds(fireDelay);
		}
	}
}
