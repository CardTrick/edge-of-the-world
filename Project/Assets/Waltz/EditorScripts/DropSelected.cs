﻿using UnityEngine;
using UnityEditor;


public class DropSelected: ScriptableObject {

	[MenuItem("Waltz/Drop Selected %#D", false, 4)]
	static void DropSelectedObjects() {
		Transform[] selected = Selection.transforms;
		foreach (Transform transform in selected) {
			DropObject(transform);
		}
	}
	
	static void DropObject(Transform transform) {
		Bounds boundingBox = GeomU.GetBoundingBox(transform);
		float currentHeight = boundingBox.min.y;
		Vector3 highestPointBeneath = Vector3.zero;
		bool started = false;
		RaycastHit[] hits = Physics.RaycastAll(transform.position, -Vector3.up);
		if (hits.Length == 0) {
			return;
		}
		foreach (RaycastHit hit in hits) {
			if (!started) {
				if (hit.point.y < currentHeight) {
					highestPointBeneath = hit.point;
					started = true;
				}
			} else {
				if (hit.point.y > highestPointBeneath.y && hit.point.y < currentHeight) {
					highestPointBeneath = hit.point;
				}
			}
		}
		if (!started) {
			return;
		}
		Vector3 basePoint = boundingBox.center - Vector3.up * (boundingBox.extents.y);
		Undo.RecordObject(transform, "Dropped object");
		transform.Translate(highestPointBeneath - basePoint);
	}

}