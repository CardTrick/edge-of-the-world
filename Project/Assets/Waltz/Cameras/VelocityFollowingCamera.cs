﻿using UnityEngine;
using System.Collections;


public class VelocityFollowingCamera: MonoBehaviour {

	public Transform cameraTarget;
	public bool alwaysTargetPlayer;

	public float distanceBehindTarget = 4.0f;
	public float distanceAboveTarget = 2.0f;
	public float lookAtHeight = 0.0f;

	public float spinningSpeed = 1.0f;
	public float zoomingSpeed = 1.0f;
	public float elevatingSpeed = 1.0f;

	Vector3 lastDirection = Vector3.forward;


	void Start() {
	
	}

	void FixedUpdate() {
		Transform target = (alwaysTargetPlayer || cameraTarget==null) ? GameObject.FindWithTag("Player").transform : cameraTarget;

		Vector3 direction = GeomU.FlatVec(target.rigidbody.velocity).normalized;
		if (direction.magnitude < 0.1f) {
			direction = lastDirection;
		} else {
			lastDirection = direction;
		}
		Vector3 camDir = GeomU.FlatVec(transform.forward).normalized;
		float spinFactor = Vector3.Cross(camDir, direction).magnitude;
		LogU.printf("dir: {0} cam: {1} factor: {2}", direction, camDir, spinFactor);

		Vector3 targetPosition = target.position - (direction*distanceBehindTarget) + (Vector3.up*distanceAboveTarget);
		transform.position = GeomU.SpinTowards(transform.position, targetPosition,
		                                       spinningSpeed*spinFactor, zoomingSpeed, elevatingSpeed);
		transform.LookAt(target.position + Vector3.up*lookAtHeight);
	}
}
