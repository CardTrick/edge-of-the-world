﻿using UnityEngine;
using UnityEditor;


public class ResetTransforms: ScriptableObject {

	[MenuItem("Waltz/Reset Placement %M", false, 0)]
	public static void ResetPlacement() {
		Transform[] selected = Selection.transforms;
		foreach (Transform transform in selected) {
			Undo.RecordObject(transform, "Reset Placement");
			transform.localPosition = Vector3.zero;
			transform.localRotation = Quaternion.identity;
		}
	}
}
