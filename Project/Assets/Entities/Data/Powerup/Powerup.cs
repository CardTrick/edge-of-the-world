﻿using UnityEngine;
using System.Collections;

public abstract class Powerup : MonoBehaviour {

	public float duration = 10.0f;


	void OnCollisionEnter(Collision collision) {
		if (collision.gameObject.layer == LayerMask.NameToLayer("Ship")) {
			ShipControl player = collision.gameObject.GetComponent<ShipControl>();
			if (player.dead) return;
			Transform display = transform.Find("Display");
			display.gameObject.SetActive(false);
			transform.Find("Particles").gameObject.SetActive(true);
			transform.parent = collision.gameObject.transform;
			//transform.localPosition = Vector3.zero;
			Destroy(GetComponent<ConstantForce>());
			Destroy(collider);
			Destroy(rigidbody);
			Destroy(GetComponent<ShipBuoyancy>());
			Destroy(GetComponent<Splasher>());
			StartCoroutine(PowerShipUp(collision.gameObject));
			MessageBox.ShowMessage(GetName(), GetColor());
		}
	}

	protected virtual IEnumerator PowerShipUp(GameObject ship) {
		yield return new WaitForSeconds(duration);
		Destroy(gameObject);
	}

	public abstract string GetName();
	public virtual Color GetColor() {
		return transform.Find("Display").renderer.sharedMaterial.color;
	}

}
