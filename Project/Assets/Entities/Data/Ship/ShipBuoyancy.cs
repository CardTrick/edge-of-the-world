﻿using UnityEngine;
using System.Collections;

public class ShipBuoyancy : MonoBehaviour {

	public float buoyancyForce = 1.0f;
	public float globalWaterHeight = 0.2f;

	bool inWater = false;


	void FixedUpdate() {
		float submergance = globalWaterHeight - transform.position.y;
		if (inWater && submergance > 0) {
			Vector3 buoyancy = -Physics.gravity + Vector3.up * buoyancyForce * submergance;
			float factor = (Director.main.bouncyMode) ? 1.75f : 1;
			rigidbody.AddForce(buoyancy*factor, ForceMode.Acceleration);
		}
	}

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag != "Water") return;
		inWater = true;
		rigidbody.drag = 1;
	}

	void OnTriggerExit(Collider other) {
		if (other.gameObject.tag != "Water") return;
		inWater = false;
		rigidbody.drag = 0;
	}

	public bool IsInWater() {
		return inWater;
	}
}
