﻿using UnityEngine;
using System.Collections;

public class MassPowerup : Powerup {

	public float mass = 50.0f;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		float oldMass = ship.rigidbody.mass;
		ship.rigidbody.mass = mass;
		yield return new WaitForSeconds(duration);
		ship.rigidbody.mass = oldMass;
		Destroy(gameObject);
	}
	
	override public string GetName() {
		return "Lead Sails ...";
	}

	override public Color GetColor() {
		return Color.gray;
	}
}
