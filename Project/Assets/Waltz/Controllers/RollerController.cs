﻿using UnityEngine;
using System.Collections;


public class RollerController: MonoBehaviour {

	public string forwardAxis = "Vertical";
	public string sideAxis = "Horizontal";

	public float acceleration = 10.0f;
	public float maxAngularVelocity = 20.0f;

	public Transform observingCamera = null;
	public bool alwaysUseMainCamera = true;


	void Start() {
		rigidbody.maxAngularVelocity = maxAngularVelocity;
	}

	void FixedUpdate() {
		Transform observer = (alwaysUseMainCamera) ? Camera.main.transform : observingCamera;
		Vector3 torque = InputU.FlatObserverTorqueInputVec(sideAxis, forwardAxis, observer);
		rigidbody.AddTorque(torque * acceleration, ForceMode.Acceleration);
	}
}
