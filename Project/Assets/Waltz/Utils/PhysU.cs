﻿using UnityEngine;
using System.Collections;


public class PhysU {

	// Returns -1 if vec is facing left, 1 if it is facing right, and 0 if its X length is under threshold.
	public static int Direction(Vector2 vec, float threshold=0.05f) {
		return MathU.FuzzySign(vec.x, threshold);
	}

	// Returns the vertical speed required to jump to height given the gravity.
	public static float JumpSpeed(float height, float gravity) {
		return Mathf.Sqrt(Mathf.Abs(2 * gravity * height));
	}

	// Returns the acceleration needed to reach a certain speed over a certain distance.
	public static float AccelForSpeedInDistance(float speed, float distance) {
		return (speed*speed) / (2*distance);
	}

	// Sets the velocity of a rigidbody, but only in certain dimensions.
	public static void SetXVelocity(Rigidbody2D rigidbody, float velocity) {
		rigidbody.velocity = new Vector2(velocity, rigidbody.velocity.y);
	}

	public static void SetXZVelocity(Rigidbody rigidbody, Vector3 velocity) {
		rigidbody.velocity = new Vector3(velocity.x, rigidbody.velocity.y, velocity.z);
	}

	public static void SetYVelocity(Rigidbody2D rigidbody, float velocity) {
		rigidbody.velocity = new Vector2(rigidbody.velocity.x, velocity);
	}

	public static void SetYVelocity(Rigidbody rigidbody, float velocity) {
		rigidbody.velocity = new Vector3(rigidbody.velocity.x, velocity, rigidbody.velocity.z);
	}
}
