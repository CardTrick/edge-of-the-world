﻿using UnityEngine;
using System.Collections;

public class MoveToEdge : MonoBehaviour {

	public Vector3 force = Vector3.zero;

	// Use this for initialization
	void Start () {
		ConstantForce f = gameObject.AddComponent("ConstantForce") as ConstantForce;
		f.force = force;
		StartCoroutine(MagnifyByIn(3.0f, 3.0f));
	}

	IEnumerator MagnifyByIn(float factor, float time) {
		yield return new WaitForSeconds(time);
		ConstantForce f = GetComponent<ConstantForce>();
		if (f != null) {
			f.force = f.force * factor;
		}
	}

}
