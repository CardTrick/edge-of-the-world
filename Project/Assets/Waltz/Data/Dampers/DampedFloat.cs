﻿using UnityEngine;
using System.Collections;


public class DampedFloat: DampedValue<float> {

	public DampedFloat(float current, float smoothTime, float initialVel=0.0f, float maxSpeed=Mathf.Infinity):
			base(current, smoothTime, initialVel, maxSpeed) {
	}

	override public float Tick(float target, float deltaTime) {
		value = Mathf.SmoothDamp(value, target, ref velocity, smoothTime, maxSpeed, deltaTime);
		return value;
	}
}
