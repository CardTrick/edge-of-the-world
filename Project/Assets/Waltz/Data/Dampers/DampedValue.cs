﻿using UnityEngine;
using System.Collections;


public abstract class DampedValue<T>: Object {

	public T value;
	public T velocity;
	public float maxSpeed;
	public float smoothTime;

	public DampedValue(T current, float smoothTime, T initialVel, float maxSpeed) {
		this.value = current;
		this.smoothTime = smoothTime;
		this.velocity = initialVel;
		this.maxSpeed = maxSpeed;
	}

	public abstract T Tick(T target, float deltaTime);
}
