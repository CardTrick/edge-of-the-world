﻿using UnityEngine;
using System.Collections;

[System.Serializable]
public class Spawnable {

	public GameObject prefab;
	public float weight;

}