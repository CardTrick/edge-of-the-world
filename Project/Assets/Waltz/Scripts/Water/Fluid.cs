﻿using UnityEngine;
using System.Collections;


public class Fluid: MonoBehaviour {

	public float density = 1000;
	public float drag = 2.0f;
	public float angularDrag = 3.0f;


	void OnTriggerEnter(Collider other) {
		Buoyant buoyant = other.GetComponent<Buoyant>();
		if (buoyant != null) {
			buoyant.SetInFluid(this);
		}
	}

	void OnTriggerExit(Collider other) {
		Buoyant buoyant = other.GetComponent<Buoyant>();
		if (buoyant != null) {
			buoyant.SetNotInFluid();
		}
	}

	void OnTriggerStay(Collider other) {
		Buoyant buoyant = other.GetComponent<Buoyant>();
		if (buoyant == null) {
			return;
		}
		int gridSize = buoyant.gridSize;
		Bounds bounds = other.bounds;
		float maxWaterHeight = collider.bounds.max.y+1.0f;
		float minObjectHeight = bounds.min.y-1.0f;
		float rayDistance = maxWaterHeight-minObjectHeight;
		Vector3 march = bounds.size / gridSize;
		Vector3 start = bounds.center - bounds.extents + (march/2);
		float segmentArea = march.x * march.z;
		float totalSubmergedHeight = 0;
		Vector3 resultantForce = Vector3.zero;
		Vector3 centerOfForce = Vector3.zero;
		for (int x=0; x<gridSize; x++) {
			for (int z=0; z<gridSize; z++) {
				float fx = start.x + (march.x*x);
				float fz = start.z + (march.z*z);
				RaycastHit waterHit;
				RaycastHit objectHit;
				bool hitWater = collider.Raycast(new Ray(new Vector3(fx, maxWaterHeight, fz), Vector3.down),
				                                 out waterHit, rayDistance);
				if (hitWater) {
					bool hitObject = other.Raycast(new Ray(new Vector3(fx, minObjectHeight, fz), Vector3.up),
					                               out objectHit, rayDistance);
					if (hitObject) {
						float height = waterHit.point.y - objectHit.point.y;
						if (height < 0) {
							continue;
						}
						totalSubmergedHeight += height;
						resultantForce += Vector3.up * -Physics.gravity.y * density * height * segmentArea;
						centerOfForce += objectHit.point*height;
					}
				}
			}
		}
		if (totalSubmergedHeight == 0) {
			buoyant.SetNotInFluid();
		} else {
			buoyant.SetInFluid(this);
			centerOfForce /= totalSubmergedHeight;
			buoyant.body.AddForceAtPosition(resultantForce, centerOfForce);
		}
	}
}
