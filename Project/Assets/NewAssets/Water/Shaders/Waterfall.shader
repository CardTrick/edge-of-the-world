﻿Shader "Custom/Waterfall" {
	Properties {
		_MainTex ("Color (RGB) Alpha (A)", 2D) = "white" {}
		_Color ("Color (RGBA)", Color) = (1,1,1,1)
		_Height ("Height", Float) = 1.0
		_Speed ("Speed", Float) = 1.0
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		ZWrite Off
		Blend SrcAlpha OneMinusSrcAlpha
		
		Pass {
			CGPROGRAM
			#pragma vertex vert
			#pragma fragment frag
			#include "UnityCG.cginc"

			sampler2D _MainTex;
			float4 _MainTex_ST;
			fixed4 _Color;
			float _Height;
			float _Speed;
			
			struct trans {
				float4 pos: SV_POSITION;
				float4 localPos: TEXCOORD1;
				float2 uv : TEXCOORD0;
			};
			
			trans vert(appdata_base v) {
				trans t;
				t.pos = mul(UNITY_MATRIX_MVP, v.vertex);
				t.localPos = v.vertex;
				t.uv = TRANSFORM_TEX(v.texcoord, _MainTex);
				return t;
			}
			
			fixed4 frag(trans t) : COLOR0 {
				float random = fmod(abs(floor(t.localPos.x)), 3.0)+1;
				float2 coord = float2(t.uv.x, t.uv.y + (_Time.y * _Speed * random));
				fixed4 col = tex2D(_MainTex, coord) * _Color;
				float amod = smoothstep(0, -_Height, t.localPos.y);
				return fixed4(col.xyz, col.a * (1-amod));
			}
			
			ENDCG
		}
	} 
	FallBack "Diffuse"
}
