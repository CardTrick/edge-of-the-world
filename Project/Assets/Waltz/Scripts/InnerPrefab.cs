﻿using UnityEngine;
using UnityEditor;
using System.Collections;


public class InnerPrefab: AbstractInnerPrefab {

	public static Color gizmoColor = new Color(1, 0.6f, 0, 1);

	public GameObject prefab;
	public bool selfDestruct = true;
	public GameObject spawnedObject = null;


	public void Awake() {
		Spawn();
		if (selfDestruct) {
			DestroyImmediate(gameObject);
		}
	}

	[ContextMenu("Spawn Object")]
	public override void Spawn() {
		if (spawnedObject != null) return;
		GameObject result = Instantiate(prefab) as GameObject;
		result.transform.parent = transform.parent;
		result.transform.localPosition = transform.localPosition;
		result.transform.localRotation = transform.localRotation;
		result.transform.localScale = Vector3.Scale(transform.localScale, result.transform.localScale);
		result.name = gameObject.name;
		spawnedObject = result;
#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
#endif
	}

	[ContextMenu("Clear Object")]
	public override void Unspawn() {
		if (spawnedObject == null) return;
		DestroyImmediate(spawnedObject);
		spawnedObject = null;
#if UNITY_EDITOR
		EditorUtility.SetDirty(this);
#endif
	}

	public void OnDrawGizmos() {
		Gizmos.color = gizmoColor;
		Gizmos.matrix = Matrix4x4.TRS(transform.position, transform.rotation, transform.lossyScale);
		Gizmos.DrawWireCube(Vector3.zero, prefab.renderer.bounds.size);
	}
}
