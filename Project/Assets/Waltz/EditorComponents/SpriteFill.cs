﻿using UnityEngine;
using UnityEditor;
using System.Collections.Generic;


public class SpriteFill: MonoBehaviour {

	public Sprite sprite;
	public Color spriteColor = Color.white;
	public string nameForChildren = "Sprite";
	public int layerForChildren = 0;
	public bool useOwnLayerInstead = true;


	[ContextMenu("Fill Collider")]
	public void FillSprites() {
		ClearSprites();
		BoxCollider2D col = GetComponent<BoxCollider2D>();
		Bounds spritebounds = sprite.bounds;
		int numWide = (int)(col.size.x/spritebounds.size.x);
		int numTall = (int)(col.size.y/spritebounds.size.y);
		Vector3 origin = transform.position - ((Vector3) (col.size/2 - col.center));
		for (int x=0; x<numWide; x++) {
			for (int y=0; y<numTall; y++) {
				GameObject newsprite = new GameObject(nameForChildren);
#if UNITY_EDITOR
				Undo.RegisterCreatedObjectUndo(newsprite, "Created sprite.");
#endif
				newsprite.transform.parent = transform;
				newsprite.layer = (useOwnLayerInstead) ? gameObject.layer : layerForChildren;
				SpriteRenderer renderer = newsprite.AddComponent("SpriteRenderer") as SpriteRenderer;
				renderer.sprite = sprite;
				renderer.color = spriteColor;
				Vector3 halfSprite = Vector3.Scale(spritebounds.extents, new Vector3(1,1,0));
				newsprite.transform.position = origin + halfSprite + Vector3.Scale(new Vector3(x,y,0), spritebounds.size);
			}
		}
#if UNITY_EDITOR
		EditorUtility.SetDirty(transform);
#endif
	}

	[ContextMenu("Clear Sprites")]
	public void ClearSprites() {
		List<GameObject> killList = new List<GameObject>();
		foreach (Transform child in transform) {
			GameObject gobj = child.gameObject;
			if (gobj.name == nameForChildren) {
				killList.Add(gobj);
			}
		}
		foreach (GameObject gobj in killList) {
#if UNITY_EDITOR
			Undo.DestroyObjectImmediate(gobj);
#endif
			DestroyImmediate(gobj);
		}
#if UNITY_EDITOR
		EditorUtility.SetDirty(transform);
#endif
	}
}
