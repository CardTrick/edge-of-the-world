﻿using UnityEngine;
using System.Collections;

public class Explosive : MonoBehaviour {

	public GameObject explosionPrefab;

	float waitTime = 0.2f;

	void Update() {
		waitTime -= Time.deltaTime;
	}

	void OnCollisionEnter(Collision collision) {
		if (waitTime > 0) return;
		ShipControl control = GetComponent<ShipControl>();
		if (control != null) {
			control.Die();
		}
		Destroy(Instantiate(explosionPrefab, transform.position, Quaternion.identity), 12.0f);
		Destroy(gameObject);
	}
}
