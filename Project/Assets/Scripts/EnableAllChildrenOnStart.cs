﻿using UnityEngine;
using System.Collections;


public class EnableAllChildrenOnStart: MonoBehaviour {
	void Start() {
		foreach (Transform t in transform) {
			t.gameObject.SetActive(true);
		}
	}
}
