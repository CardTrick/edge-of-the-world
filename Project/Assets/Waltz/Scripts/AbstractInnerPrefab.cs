﻿using UnityEngine;
using System.Collections;


public class AbstractInnerPrefab: MonoBehaviour {
	
	public virtual void Spawn() {}
	
	public virtual void Unspawn() {}
}
