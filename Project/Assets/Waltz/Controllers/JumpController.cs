﻿using UnityEngine;
using System.Collections;


public abstract class JumpController: MonoBehaviour {

	protected bool grounded = false;

	public void SetGrounded(bool onGround) {
		if (grounded && !onGround) {
			OnLeaveGround();
		}
		if (!grounded && onGround) {
			OnTouchGround();
		}
		grounded = onGround;
	}

	public bool IsGrounded() {
		return grounded;
	}

	protected virtual void OnTouchGround() {}
	protected virtual void OnLeaveGround() {}

}
