﻿using UnityEngine;
using UnityEditor;


public class ShowHideCloth: ScriptableObject {

	[MenuItem("Waltz/Hide Interactive Cloth &C", false, 16)]
	public static void HideCloth() {
		InteractiveCloth[] cloths = Resources.FindObjectsOfTypeAll<InteractiveCloth>();
		foreach (InteractiveCloth cloth in cloths) {
			cloth.gameObject.SetActive(false);
		}
	}

	[MenuItem("Waltz/Show Interactive Cloth #&C", false, 17)]
	public static void ShowCloth() {
		InteractiveCloth[] cloths = Resources.FindObjectsOfTypeAll<InteractiveCloth>();
		foreach (InteractiveCloth cloth in cloths) {
			cloth.gameObject.SetActive(true);
		}
	}
}
