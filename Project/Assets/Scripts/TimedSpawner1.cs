﻿using UnityEngine;
using System.Collections;

public class TimedSpawner1 : MonoBehaviour {

	public GameObject prefab;
	public GameObject[] gimmicks;

	public float minTime = 4.0f;
	public float maxTime = 14.0f;
	public float gimmickChance = 0.04f;
	public float objectLifetime = 5.0f;


	void Start() {
		StartCoroutine(SpawnForever());
	}
	
	IEnumerator SpawnForever() {
		while (true) {
			float waitTime = minTime + Random.value * (maxTime-minTime);
			yield return new WaitForSeconds(waitTime);
			GameObject obj = (Random.value < gimmickChance) ? gimmicks[Random.Range(0, gimmicks.Length)] : prefab;
			Destroy(Instantiate(obj, transform.position, Random.rotation), objectLifetime);
		}
	}
}
