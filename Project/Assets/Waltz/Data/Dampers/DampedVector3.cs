﻿using UnityEngine;
using System.Collections;


public class DampedVector3: DampedValue<Vector3> {

	public DampedVector3(Vector3 current, float smoothTime, Vector3 initialVel=default(Vector3), float maxSpeed=Mathf.Infinity):
			base(current, smoothTime, initialVel, maxSpeed) {
	}

	override public Vector3 Tick(Vector3 target, float deltaTime) {
		value = Vector3.SmoothDamp(value, target, ref velocity, smoothTime, maxSpeed, deltaTime);
		return value;
	}
}
