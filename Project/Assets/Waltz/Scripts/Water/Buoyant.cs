﻿using UnityEngine;
using System.Collections;


public class Buoyant: MonoBehaviour {
	
	public int gridSize = 1;

	public Rigidbody body = null;
	public bool useOwnRigidbody = true;
	
	float normalDrag;
	float normalAngDrag;
	bool inFluid;


	void Start() {
		if (body == null || useOwnRigidbody) {
			body = FindNearestRigidbody();
		}
		normalDrag = body.drag;
		normalAngDrag = body.angularDrag;
	}

	Rigidbody FindNearestRigidbody() {
		if (GetComponent<Rigidbody>() != null) {
			return rigidbody;
		}
		Transform next = transform.parent;
		while (next != null) {
			Rigidbody nextBody = next.GetComponent<Rigidbody>();
			if (nextBody != null) {
				return nextBody;
			}
			next = next.parent;
		}
		return null;
	}

	public void SetInFluid(Fluid fluid) {
		if (inFluid) return;
		inFluid = true;
		body.drag = normalDrag + fluid.drag;
		body.angularDrag = normalAngDrag + fluid.angularDrag;
	}

	public void SetNotInFluid() {
		if (!inFluid) return;
		inFluid = false;
		body.drag = normalDrag;
		body.angularDrag = normalAngDrag;
	}

	public bool IsInFluid() {
		return inFluid;
	}
}
