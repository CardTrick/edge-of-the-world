﻿using UnityEngine;
using System.Collections;


public class ChildCenterOfMass: MonoBehaviour {

	public string centerOfMassName = "CenterOfMass";


	void Start() {
		rigidbody.centerOfMass = transform.Find(centerOfMassName).localPosition;
	}
}
