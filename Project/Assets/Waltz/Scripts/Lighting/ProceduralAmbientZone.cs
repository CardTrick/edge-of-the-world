﻿using UnityEngine;
using System.Collections;


public class ProceduralAmbientZone: MonoBehaviour {

	public static Color gizmoColor = new Color(0.5f, 0.75f, 1, 1);

	public bool changeBackOnExit = true;
	
	public bool onlyTriggeredByTag = true;
	public string triggerTag = "Player";

	public Color centerColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
	public float radius = 0.5f;

	Color revertColor;

	public float effectiveRadius { get { return radius*transform.lossyScale.z; } }


	void OnTriggerEnter(Collider other) {
		if (!onlyTriggeredByTag || other.gameObject.tag == triggerTag) {
			revertColor = AmbienceController.main.ambientLight;
		}
	}

	void OnTriggerExit(Collider other) {
		if (changeBackOnExit && (!onlyTriggeredByTag || other.gameObject.tag == triggerTag)) {
			AmbienceController.main.ambientLight = revertColor;
		}
	}

	void OnTriggerStay(Collider other) {
		if (!onlyTriggeredByTag || other.gameObject.tag == triggerTag) {
			Vector3 disp = other.transform.position - transform.position;
			float t = disp.magnitude;
			float fade = t / effectiveRadius;
			AmbienceController.main.ambientLight = Color.Lerp(centerColor, revertColor, fade);
		}
	}

	void OnDrawGizmosSelected() {
		Gizmos.color = gizmoColor;
		Gizmos.DrawWireSphere(transform.position, effectiveRadius);
	}
}