﻿using UnityEngine;
using System.Collections;


public class Spawner: MonoBehaviour {

	public GameObject spawnLibrary;


	protected GameObject Spawn(Vector3 position=default(Vector3), Quaternion rotation=default(Quaternion)) {
		return Instantiate(PickSpawn(), position, rotation) as GameObject;
	}

	protected GameObject SpawnHere() {
		return Instantiate(PickSpawn(), transform.position, transform.rotation) as GameObject;
	}

	protected GameObject PickSpawn() {
		SpawnLibrary lib = spawnLibrary.GetComponent<SpawnLibrary>();
		if (lib == null) return null;
		float totalWeight = 0;
		foreach (Spawnable spawnable in lib.spawnables) {
			totalWeight += spawnable.weight;
		}
		float r = Random.value * totalWeight;
		float p = 0;
		foreach (Spawnable spawnable in lib.spawnables) {
			p += spawnable.weight;
			if (r <= p) {
				return spawnable.prefab;
			}
		}
		return null;
	}
}
