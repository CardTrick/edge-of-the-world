﻿Shader "Waltz/SimpleTransparent" {
	Properties {
		_MainTex ("Base (RGBA)", 2D) = "white" {}
		_Color ("Color (RGBA)", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Transparent" "Queue"="Transparent" }
		LOD 200
		
		Pass {
			Lighting Off
			Blend SrcAlpha OneMinusSrcAlpha
			ZWrite Off
			
			SetTexture [_MainTex] {
				constantColor [_Color]
				combine constant * texture
			}
		}
	} 
	FallBack "Diffuse"
}
