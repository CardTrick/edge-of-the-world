﻿using UnityEngine;
using System.Collections;


public class ImpulseJumpController: JumpController {

	public int jumps = 1;
	public float jumpHeight = 2.0f;
	public float jumpPreloadTime = 0.2f;

	public string jumpButton = "Jump";

	int jumpsRemaining = 0;
	float jumpSpeed;
	float preloadTime = 0;


	void Start() {
		jumpSpeed = PhysU.JumpSpeed(jumpHeight, Physics.gravity.y);
	}

	void Update() {
		bool buttonPressed = Input.GetButtonDown(jumpButton);
		if (jumpsRemaining > 0 && (buttonPressed || preloadTime > 0)) {
			PhysU.SetYVelocity(rigidbody, jumpSpeed);
			jumpsRemaining -= 1;
			preloadTime = 0;
		} else if (buttonPressed) {
			preloadTime = jumpPreloadTime;
		}
		preloadTime = (preloadTime>Time.deltaTime) ? (preloadTime-Time.deltaTime) : 0;
	}

	override protected void OnTouchGround() {
		jumpsRemaining = jumps;
	}
}
