﻿using UnityEngine;
using System.Collections;

public class FirstPersonCam : MonoBehaviour {

	string axis;
	string vertAxis;


	void Start () {
		ShipControl player = transform.parent.GetComponent<ShipControl>();
		SetupViewport(player.playerNumber, Director.main.GetPlayerCount());
		axis = "Player"+player.playerNumber+"Look";
		vertAxis = "Player"+player.playerNumber+"LookVert";
	}

	void SetupViewport(int thisPlayer, int totalPlayers) {
		float width = 1.0f / totalPlayers;
		float height = 1.0f;
		float x = 0;
		float y = 0;
		switch (thisPlayer) {
		case 1: x = 0; break;
		case 2: x = 1-width; break;
		case 3: x = width; break;
		}
		camera.rect = new Rect(x,y,width,height);
	}

	void Update() {
		float input = Input.GetAxis(axis);
		float vinput = Input.GetAxis(vertAxis);
		transform.localEulerAngles = new Vector3(vinput*45, input*135, 0);
	}
}
