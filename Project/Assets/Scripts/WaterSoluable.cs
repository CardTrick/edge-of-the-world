﻿using UnityEngine;
using System.Collections;

public class WaterSoluable : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Water") {
			Destroy(gameObject, 0.1f);
		}
	}
}
