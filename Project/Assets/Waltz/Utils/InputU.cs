﻿using UnityEngine;
using System.Collections;


public class InputU {

	// Returns a 3D vector representing the position of a 2D joystick.
	public static Vector3 InputVec(string axisX, string axisZ) {
		return new Vector3(Input.GetAxis(axisX), 0, Input.GetAxis(axisZ));
	}

	// Returns a 3D vector representing the position of a 2D joystick, never exceeding a magnitude of 1.
	public static Vector3 ClampedInputVec(string xAxis, string zAxis) {
		Vector3 inputVec = InputVec(xAxis, zAxis);
		if (inputVec.sqrMagnitude > 1.0) {
			Vector3.Normalize(inputVec);
		}
		return inputVec;
	}

	// Returns a 2D vector representing the position of a 2D joystick.
	public static Vector2 InputVec2D(string axisX, string axisY) {
		return new Vector2(Input.GetAxis(axisX), Input.GetAxis(axisY));
	}

	// Returns an input vector in an observers local space (Main Camera by default). The Y coordinate is always 0.
	public static Vector3 FlatObserverInputVec(string sideAxis, string forwardAxis, Transform observer=null) {
		if (observer == null) observer = Camera.main.transform;
		Vector3 right = observer.transform.right;
		Vector3 forwards = Vector3.Cross(right, Vector3.up);
		return (right * Input.GetAxis(sideAxis)) + (forwards * Input.GetAxis(forwardAxis));
	}
	
	public static Vector3 FlatObserverTorqueInputVec(string sideAxis, string forwardAxis, Transform observer=null) {
		if (observer == null) observer = Camera.main.transform;
		Vector3 right = observer.transform.right;
		Vector3 forwards = Vector3.Cross(right, Vector3.up);
		return (forwards * -Input.GetAxis(sideAxis)) + (right * Input.GetAxis(forwardAxis));
	}

	// Returns an input vector in an observers local space (Main Camera by default).
	public static Vector3 ObserverInputVec(string sideAxis, string forwardAxis, Transform observer=null) {
		if (observer == null) observer = Camera.main.transform;
		return observer.TransformDirection(InputVec(sideAxis, forwardAxis));
	}

}
