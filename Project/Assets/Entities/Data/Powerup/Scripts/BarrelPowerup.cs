﻿using UnityEngine;
using System.Collections;

public class BarrelPowerup : Powerup {

	public GameObject barrelPrefab;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		Cannons cannons = ship.GetComponent<Cannons>();
		GameObject cannonball = cannons.cannonballPrefab;
		cannons.cannonballPrefab = barrelPrefab;
		yield return new WaitForSeconds(duration);
		cannons.cannonballPrefab = cannonball;
		Destroy(gameObject);
	}

	override public string GetName() {
		return "Powder Keg Cannon";
	}
}
