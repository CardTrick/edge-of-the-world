﻿using UnityEngine;
using System.Collections;

public class PlayerTimer : MonoBehaviour {

	public int playerNumber = 1;
	ShipControl player;
	TextMesh text;

	GameObject skull;
	bool dead = false;


	void Start() {
		player = Director.main.GetPlayer(playerNumber);
		text = GetComponent<TextMesh>();
		skull = transform.Find("Skull").gameObject;
	}
	
	// Update is called once per frame
	void Update () {
		float time = player.GetLifetime();
		int minutes = (int)(time / 60);
		int seconds = (int)(time - (minutes*60));
		int centiseconds = (int)((time - Mathf.Floor(time)) * 100);
		text.text = string.Format("{0}:{1}:{2}", minutes.ToString("D2"), seconds.ToString("D2"), centiseconds.ToString("D2"));
		if (player.dead && !dead) {
			Die();
		}
	}

	void Die() {
		skull.SetActive(true);
		dead = true;
	}
}
