﻿using UnityEngine;
using System.Collections;

public class Deathzone : MonoBehaviour {

	void OnTriggerEnter(Collider other) {
		ShipControl controller = other.GetComponent<ShipControl>();
		if (controller != null) {
			controller.Die();
		} else {
			Destroy(other.transform.root.gameObject, 3.0f);
		}
	}
}
