﻿using UnityEngine;
using System.Collections;

public class RangePowerup : Powerup {

	public float newForce;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		Cannons cannons = ship.GetComponent<Cannons>();
		float oldforce = cannons.force;
		cannons.force = newForce;
		yield return new WaitForSeconds(duration);
		cannons.force = oldforce;
		Destroy(gameObject);
	}	
	
	override public string GetName() {
		return "Long Range Cannon";
	}
}
