﻿using UnityEngine;
using System.Collections;

public class Splasher : MonoBehaviour {

	public GameObject splashPrefab;

	public float minSpeedForSplash = 0.0f;
	public float globalWaterLevel = 0.2f;


	void OnTriggerEnter(Collider other) {
		if (other.gameObject.tag == "Water" && -rigidbody.velocity.y > minSpeedForSplash) {
			Vector3 pos = transform.position;
			pos.y = globalWaterLevel;
			Destroy(Instantiate(splashPrefab, pos, Quaternion.identity), 2.0f);
		}
	}
}
