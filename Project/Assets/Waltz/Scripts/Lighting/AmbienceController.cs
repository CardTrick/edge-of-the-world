﻿using UnityEngine;
using System.Collections;


public class AmbienceController: MonoBehaviour {

	static AmbienceController _main;
	public static AmbienceController main {
		get {
			return _main;
		}
	}

	public Color initialAmbientLight = new Color(0.5f, 0.5f, 0.5f, 1.0f);
	public Color ambientLight {
		get {
			return RenderSettings.ambientLight;
		}
		set {
			RenderSettings.ambientLight = value;
		}
	}


	void Start() {
		_main = this;
		ResetAmbientLight();
	}

	[ContextMenu("Reset Ambient Light")]
	public void ResetAmbientLight() {
		ambientLight = initialAmbientLight;
	}

	public void FadeTo(Color color, float time) {
		StopAllCoroutines();
		StartCoroutine(CoFadeTo(color, time));
	}

	IEnumerator CoFadeTo(Color color, float time) {
		float elapsed = 0;
		Color from = ambientLight;
		while (elapsed < time) {
			ambientLight = Color.Lerp(from, color, elapsed/time);
			elapsed += Time.deltaTime;
			yield return null;
		}
		ambientLight = color;
	}
}
