﻿using UnityEngine;
using System.Collections;


public class AmbientZone: MonoBehaviour {
	
	public float transitionTime = 1.0f;
	public bool changeBackOnExit = true;

	public bool onlyTriggeredByTag = true;
	public string triggerTag = "Player";
	public Color lightColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);

	Color revertColor;


	void OnTriggerEnter(Collider other) {
		if (!onlyTriggeredByTag || other.gameObject.tag == triggerTag) {
			revertColor = AmbienceController.main.ambientLight;
			AmbienceController.main.FadeTo(lightColor, transitionTime);
		}
	}

	void OnTriggerExit(Collider other) {
		if (changeBackOnExit && (!onlyTriggeredByTag || other.gameObject.tag == triggerTag)) {
			AmbienceController.main.FadeTo(revertColor, transitionTime);
		}
	}
}
