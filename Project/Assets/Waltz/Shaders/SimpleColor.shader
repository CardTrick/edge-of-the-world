﻿Shader "Waltz/SimpleColor" {
	Properties {
		_MainTex ("Base (RGB)", 2D) = "white" {}
		_Color ("Color (RGB)", Color) = (1,1,1,1)
	}
	SubShader {
		Tags { "RenderType"="Opaque" }
		LOD 200
		
		Pass {
			Lighting Off
			
			SetTexture [_MainTex] {
				constantColor [_Color]
				combine constant * texture
			}
		}
	} 
	FallBack "Diffuse"
}
