﻿using UnityEngine;
using System.Collections;


public class DirectionalAmbientZone: MonoBehaviour {

	public float transitionTime = 1.0f;
	
	public bool onlyTriggeredByTag = true;
	public string triggerTag = "Player";

	public Color frontLightColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
	public Color backLightColor = new Color(0.5f, 0.5f, 0.5f, 1.0f);
	
	
	void OnTriggerEnter(Collider other) {
		if (!onlyTriggeredByTag || other.gameObject.tag == triggerTag) {
			Vector3 disp = other.transform.position - transform.position;
			Color color = (Vector3.Dot(disp, transform.forward) > 0) ? frontLightColor : backLightColor;
			AmbienceController.main.FadeTo(color, transitionTime);
		}
	}
}
