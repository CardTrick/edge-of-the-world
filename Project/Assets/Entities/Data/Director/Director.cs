﻿using UnityEngine;
using System.Collections;

public class Director : MonoBehaviour {

	public static Director main {
		get { return GameObject.FindWithTag("GameController").GetComponent<Director>(); }
	}

	public Material silver;
	public Material ghost;
	public Material normalShips;

	public bool silverShipsMode = false;
	public bool ghostShipsMode = false;
	public bool firstPersonMode = false;
	public bool fastMode = false;
	public bool bouncyMode = false;
	public bool running = true;


	ShipControl[] players;
	GameObject victoryBox;
	ShipControl winner = null;


	void Awake() {
		ShipControl[] playerControllers = FindObjectsOfType(typeof(ShipControl)) as ShipControl[];
		players = new ShipControl[playerControllers.Length];
		foreach (ShipControl control in playerControllers) {
			players[control.playerIndex] = control;
		}
		victoryBox = GameObject.Find("VictoryBox");
		if (victoryBox) {
			victoryBox.SetActive(false);
		}
	}

	void Update() {
		if (Input.GetKeyDown("f2")) {
			ToggleFPMode();
		}
		if (Input.GetKeyDown("f4")) {
			bouncyMode = !bouncyMode;
		}
		if (Input.GetKeyDown("f5")) {
			Application.LoadLevel(Application.loadedLevel);
		}
		if (Input.GetKeyDown("f7")) {
			ToggleGhostShipsMode();
		}
		if (Input.GetKeyDown("f8")) {
			ToggleFastMode();
		}
		if (Input.GetKeyDown("f9")) {
			ToggleSilverShipsMode();
		}
	}

	public ShipControl GetPlayer(int i) {
		return players[i-1];
	}

	public int GetPlayerCount() {
		return players.Length;
	}

	public void CheckForVictory() {
		if (!running) {
			return;
		}
		int numberAlive = 0;
		ShipControl lastAlive = null;
		foreach (ShipControl player in players) {
			if (!player.dead) {
				numberAlive += 1;
				lastAlive = player;
			}
		}
		if (numberAlive == 1) {
			winner = lastAlive;
			StartCoroutine(FinishMatch());
		} else if (numberAlive == 0) {
			winner = null;
			StartCoroutine(FinishMatch());
		}
	}

	IEnumerator FinishMatch() {
		yield return new WaitForSeconds(1.0f);
		victoryBox.SetActive(true);
		victoryBox.GetComponent<VictoryBox>().SetVictor(winner);
		running = false;
		fastMode = true;
		ToggleFastMode();
	}

	void ToggleFPMode() {
		firstPersonMode = !firstPersonMode;
		foreach (ShipControl player in players) {
			if (player != null) {
				GameObject cam = player.transform.Find("First Person Camera").gameObject;
				cam.SetActive(firstPersonMode);
			}
		}
	}

	void ToggleFastMode() {
		fastMode = !fastMode;
		if (fastMode) {
			Time.timeScale = 4.0f;
		} else {
			Time.timeScale = 1.0f;
		}
	}

	void ToggleSilverShipsMode() {
		silverShipsMode = !silverShipsMode;
		ToggleShipMaterial(silver, silverShipsMode);
	}

	void ToggleGhostShipsMode() {
		ghostShipsMode = !ghostShipsMode;
		ToggleShipMaterial(ghost, ghostShipsMode);
		Vector3 disp = (ghostShipsMode) ? Vector3.up : Vector3.down;
		foreach (ShipControl player in players) {
			player.transform.Find("Display").localPosition += disp;
			player.transform.Find("First Person Camera").localPosition += disp;
		}
	}

	void ToggleShipMaterial(Material mat, bool enabled) {
		foreach (ShipControl player in players) {
			if (player != null) {
				Renderer renderer = player.transform.Find("Display").renderer;
				if (enabled) {
					renderer.sharedMaterial = mat;
				} else {
					renderer.sharedMaterial = normalShips;
				}
			}
		}
	}
}
