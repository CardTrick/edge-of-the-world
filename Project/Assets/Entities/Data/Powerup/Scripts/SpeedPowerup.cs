﻿using UnityEngine;
using System.Collections;

public class SpeedPowerup : Powerup {

	public float sailingUp = 2.0f;
	public float turningUp = 1.0f;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		ShipControl player = ship.GetComponent<ShipControl>();
		player.sailingSpeed += sailingUp;
		player.turningSpeed += turningUp;
		yield return new WaitForSeconds(duration);
		player.sailingSpeed -= sailingUp;
		player.turningSpeed -= turningUp;
		Destroy(gameObject);
	}

	override public string GetName() {
		return "The Interceptor";
	}
}
