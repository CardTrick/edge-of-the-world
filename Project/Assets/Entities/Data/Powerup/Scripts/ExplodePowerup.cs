﻿using UnityEngine;
using System.Collections;

public class ExplodePowerup : Powerup {

	public GameObject explosionPrefab;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		Explosive exp = ship.AddComponent("Explosive") as Explosive;
		exp.explosionPrefab = explosionPrefab;
		yield return new WaitForSeconds(duration);
		exp = ship.GetComponent<Explosive>();
		if (exp != null) {
			Destroy(exp);
		}
		Destroy(gameObject);
	}

	override public string GetName() {
		return "Leaking Gunpowder...";
	}
}
