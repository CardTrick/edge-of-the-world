﻿using UnityEngine;
using System.Collections;

public class VictoryBox : MonoBehaviour {

	public static string[] rematchButtons = new string[] {
		"Start",
		"Player1Start", "Player2Start", "Player3Start",
		"Player1Cannons", "Player2Cannons", "Player3Cannons", 
	};

	public static string[] cancelButtons = new string[] {
		"Cancel",
		"Player1Cancel", "Player2Cancel", "Player3Cancel"
	};

	public void SetVictor(ShipControl winner) {
		if (winner == null) {
			transform.Find("Stalemate").gameObject.SetActive(true);
			return;
		}
		string label = "Player"+winner.playerNumber+"Victory";
		transform.Find(label).gameObject.SetActive(true);
	}

	void Update() {
		foreach (string rematch in rematchButtons) {
			if (Input.GetButtonDown(rematch)) {
				Application.LoadLevel(Application.loadedLevel);
				break;
			}
		}
		foreach (string cancel in cancelButtons) {
			if (Input.GetButtonDown(cancel)) {
				Application.LoadLevel(0);
			}
		}
	}
}
