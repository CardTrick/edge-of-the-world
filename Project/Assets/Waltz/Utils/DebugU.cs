﻿using UnityEngine;
using System.Collections;


public class DebugU {

	public static void DrawPoint(Vector3 point, Color color, float duration=0, bool depthTest=true) {
		Vector3 point2 = new Vector3(point.x, point.y+0.1f, point.z);
		Debug.DrawLine(point, point2, color, duration, depthTest);
	}

	public static void DrawFancyPoint(Vector3 point, Color color, float duration=0, bool depthTest=true) {
		Debug.DrawLine(new Vector3(point.x-0.1f, point.y, point.z),
		               new Vector3(point.x+0.1f, point.y, point.z),
		               color, duration, depthTest);
		Debug.DrawLine(new Vector3(point.x, point.y-0.1f, point.z),
		               new Vector3(point.x, point.y+0.1f, point.z),
		               color, duration, depthTest);
		Debug.DrawLine(new Vector3(point.x, point.y, point.z-0.1f),
		               new Vector3(point.x, point.y, point.z+0.1f),
		               color, duration, depthTest);
	}
}
