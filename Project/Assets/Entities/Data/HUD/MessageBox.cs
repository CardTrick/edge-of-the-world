﻿using UnityEngine;
using System.Collections;

public class MessageBox : MonoBehaviour {

	public static void ShowMessage(string message, Color color) {
		GameObject.FindWithTag("MessageBox").GetComponent<MessageBox>().PushMessage(message, color);
	}

	TextMesh text;


	void Start() {
		text = GetComponent<TextMesh>();
	}

	public void PushMessage(string message, Color color) {
		StopAllCoroutines();
		text.text = message;
		text.color = color;
		StartCoroutine(FadeOut(color, 2.0f));
	}

	IEnumerator FadeOut(Color color, float time) {
		float elapsed = 0;
		while (elapsed < time) {
			float alpha = 1 - elapsed/time;
			text.color = new Color(color.r, color.g, color.b, alpha);
			elapsed += Time.deltaTime;
			yield return null;
		}
		text.color = new Color(1,1,1,0);
	}
}
