﻿using UnityEngine;
using System.Collections;


public class MathU {

	// Returns 1 for positives, -1 for negatives, and 0 for zero.
	public static int Sign(float number) {
		if (number == 0) {
			return 0;
		}
		return (number < 0) ? -1 : 1;
	}

	// Like Sign, but if the absolute value of the number is less than threshold, zero is returned.
	public static int FuzzySign(float number, float threshold=0) {
		if (-threshold <= number && number <= threshold) {
			return 0;
		}
		return (number < 0) ? -1 : 1;
	}

}
