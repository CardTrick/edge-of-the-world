﻿using UnityEngine;
using System.Collections;


public class TimedSpawner: Spawner {

	public float minTimeUntilSpawn = 1.0f;
	public float maxTimeUntilSpawn = 5.0f;
	public float chanceOfSpawn = 1.0f;


	void Start() {
		StartCoroutine(SpawnForever());
	}

	IEnumerator SpawnForever() {
		while (true) {
			float waitTime = minTimeUntilSpawn + (maxTimeUntilSpawn - minTimeUntilSpawn) * Random.value;
			yield return new WaitForSeconds(waitTime);
			if (Random.value <= chanceOfSpawn) {
				SpawnHere();
			}
		}
	}
}
