﻿using UnityEngine;
using System.Collections;

public class Explosion : MonoBehaviour {

	public float force = 50.0f;
	public float radius = 4.0f;


	void OnTriggerEnter(Collider other) {
		if (other.rigidbody != null) {
			other.rigidbody.AddExplosionForce(force, transform.position, radius, 0.5f);
		}
	}
}
