﻿using UnityEngine;
using System.Collections;


public class LayerU {

	public static int defaultLayer = Layer("Default");
	public static int transparentFXLayer = Layer("TransparentFX");
	public static int waterLayer = Layer("Water");
	public static int ignoreRaycastLayer = Layer("Ignore Raycast");
	public static int norayLayer = ignoreRaycastLayer;
	
	public static int defaultMask = Mask("Default");
	public static int transparentFXMask = Mask("TransparentFX");
	public static int waterMask = Mask("Water");
	public static int ignoreRaycastMask = Mask("Ignore Raycast");
	public static int norayMask = ignoreRaycastMask;
	public static int rayMask = ~norayMask;


	// Returns the layer number of a layer
	public static int Layer(string name) {
		return LayerMask.NameToLayer(name);
	}

	// Returns a mask for all the layers given
	public static int Mask(params string[] names) {
		int mask = 0;
		foreach (string name in names) {
			mask |= (1 << Layer(name));
		}
		return mask;
	}

	// Returns a mask for all layers except those given.
	public static int NotMask(params string[] names) {
		return ~Mask(names);
	}

	// Returns a mask for all layers that collide with the given one.
	public static int CollisionMask(int layer) {
		int mask = 0;
		for (int i=0; i<32; i++) {
			mask |= (((Physics.GetIgnoreLayerCollision(layer, i)) ? 0 : 1) << i);
		}
		return mask;
	}

	public static int CollisionMask(string name) {
		return CollisionMask(Layer(name));
	}

	public static int CollisionMask2D(int layer) {
		int mask = 0;
		for (int i=0; i<32; i++) {
			mask |= (((Physics2D.GetIgnoreLayerCollision(layer, i)) ? 0 : 1) << i);
		}
		return mask;
	}

	public static int CollisionMask2D(string name) {
		return CollisionMask(Layer(name));
	}

	// Sets the layer for a Transform and all its children.
	public static void SetLayerRecursive(Transform transform, int layer) {
		transform.gameObject.layer = layer;
		foreach (Transform t in transform) {
			SetLayerRecursive(t, layer);
		}
	}

	public static void SetLayerRecursive(Transform transform, string name) {
		SetLayerRecursive(transform, Layer(name));
	}
}
