﻿using UnityEngine;
using System.Collections;

public class Ship : MonoBehaviour {

	public Vector3 centerOfMassOffset = Vector3.zero;


	void Start() {
		rigidbody.centerOfMass = rigidbody.centerOfMass + centerOfMassOffset;
		transform.Find("Display/Sails").gameObject.SetActive(true);
	}

	void OnCollisionEnter(Collision collision) {
		GameObject other = collision.gameObject;
		if (other.layer == LayerMask.NameToLayer("Ship")) {
			float energy = rigidbody.velocity.magnitude * 5.0f;
			float perHit = energy / collision.contacts.Length;
			foreach (ContactPoint contact in collision.contacts) {
				collision.gameObject.rigidbody.AddForce(contact.normal * -perHit, ForceMode.VelocityChange);
			}
		}
	}
}
