﻿using UnityEngine;
using System.Collections;

public class ScalePowerup : Powerup {

	public float scale = 2.0f;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		ship.transform.localScale *= scale;
		yield return new WaitForSeconds(duration);
		ship.transform.localScale /= scale;
		Destroy(gameObject);
	}
	
	override public string GetName() {
		return (scale < 1) ? "Tiny Ship" : "Giant Ship";
	}
}
