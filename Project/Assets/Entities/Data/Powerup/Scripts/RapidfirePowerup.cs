﻿using UnityEngine;
using System.Collections;

public class RapidfirePowerup : Powerup {

	public float newReloadTime = 0.3f;


	override protected IEnumerator PowerShipUp(GameObject ship) {
		Cannons cannons = ship.GetComponent<Cannons>();
		float oldTime = cannons.reloadTime;
		cannons.reloadTime = newReloadTime;
		yield return new WaitForSeconds(duration);
		cannons.reloadTime = oldTime;
		Destroy(gameObject);
	}
	
	override public string GetName() {
		return "Semi-automatic Cannon";
	}
}
