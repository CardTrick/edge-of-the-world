﻿using UnityEngine;
using System.Collections;


public class ParentU {

	// Parents a Transform to a parent named parentName. Returns true if the parent exists.
	public static bool To(string parentName, Transform transform) {
		GameObject parent = GameObject.Find(parentName);
		if (parent == null) {
			return false;
		} else {
			transform.parent = parent.transform;
			return true;
		}
	}

	// Parents a bunch of Transforms to a parent named parentName. Returns true if the parent exists.
	public static bool AllTo(string parentName, params Transform[] transforms) {
		GameObject parent = GameObject.Find(parentName);
		if (parent == null) {
			return false;
		} else {
			foreach (Transform transform in transforms) {
				transform.parent = parent.transform;
			}
			return true;
		}
	}
}