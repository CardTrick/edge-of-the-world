﻿using UnityEngine;
using System.Collections;

public class MenuOption : MonoBehaviour {

	public Material selectedMaterial;
	public Material unselectedMaterial;

	public float unselectedDistanceBack = 2.0f;
	public float transitionTime = 0.3f;
	
	Vector3 selectedPos;
	Vector3 unselectedPos;


	void Awake() {
		selectedPos = transform.position;
		unselectedPos = transform.position - transform.forward*unselectedDistanceBack;
		transform.position = unselectedPos;
		renderer.material = unselectedMaterial;
	}

	public void SetSelected() {
		StartCoroutine(MoveOverTime(selectedPos, transitionTime));
		renderer.material = selectedMaterial;
	}

	public void SetUnselected() {
		StartCoroutine(MoveOverTime(unselectedPos, transitionTime));
		renderer.material = unselectedMaterial;
	}

	IEnumerator MoveOverTime(Vector3 position, float time) {
		Vector3 from = transform.position;
		float elapsed = 0;
		while (elapsed < time) {
			transform.position = Vector3.Lerp(from, position, elapsed/time);
			elapsed += Time.deltaTime;
			yield return null;
		}
		transform.position = position;
	}
}
